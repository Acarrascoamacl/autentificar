package com.example.autentificar.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.autentificar.dto.LoginDTO;
import com.example.autentificar.dto.UserDTO;
import com.example.autentificar.interfaces.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {
	@Autowired
	private AuthService authService;

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> logIn(@RequestBody LoginDTO login) {
		Boolean response = this.authService.login(login);
		return new ResponseEntity<Boolean>(response, HttpStatus.ACCEPTED);
	}

	@PostMapping(value = "/add-user", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO dto){
		UserDTO response = this.authService.addUser(dto);
		return new ResponseEntity<UserDTO> (response, HttpStatus.CREATED);
		
	}
}
