package com.example.autentificar.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.autentificar.dto.LoginDTO;
import com.example.autentificar.entities.User;

public interface UserRepository extends MongoRepository<User, Integer> {
	User findByLogin(LoginDTO login);
}
