package com.example.autentificar.interfaces;

import com.example.autentificar.dto.LoginDTO;
import com.example.autentificar.dto.UserDTO;

public interface AuthService {
	Boolean login(LoginDTO login);
	
	UserDTO addUser(UserDTO user);
}
