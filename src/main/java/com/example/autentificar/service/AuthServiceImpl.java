package com.example.autentificar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.autentificar.dto.LoginDTO;
import com.example.autentificar.dto.UserDTO;
import com.example.autentificar.entities.User;
import com.example.autentificar.interfaces.AuthService;
import com.example.autentificar.repository.UserRepository;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Boolean login(LoginDTO login) {

		User entity = this.userRepository.findByLogin(login);

		UserDTO dto = this.getUserDTO(entity);
		 if(dto != null) {
			 return true;
		 }
		 return false;
	}

	private UserDTO getUserDTO(User entity) {
 
		if(entity == null) {
			
			return null;
		}
		UserDTO response = new UserDTO();
		response.setEmail(entity.getEmail());
		response.setName(entity.getName());
		response.setId(entity.getId());
		response.setLogin(entity.getLogin());

		return response;
	}

	@Override
	public UserDTO addUser(UserDTO user) {
		User entity = this.getUser(user);
		User userEntity = this.userRepository.insert(entity);
		if ( userEntity != null) {
			return this.getUserDTO(userEntity );
		}
	
		return null;
	}

	private User getUser(UserDTO user) {
		User response = new User();
		if(user == null) {
			return null;
		}
		response.setEmail(user.getEmail());
		response.setName(user.getName());
		response.setId(user.getId());
		response.setLogin(user.getLogin());
		return response;
	}

}
